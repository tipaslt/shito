

const menus = (function(){
    let Menu = function (id) {
        this.id = id;
    };
    let data = {
        allItems: [],
    };
	return{

		addMenu: function(){
			let newMenu,ID;
			if(data.allItems.length > 0){
			    ID = data.allItems[data.allItems.length- 1].id + 1;
			}
			else{
			    ID = 0;
            }
            newMenu = new Menu(ID);
            data.allItems.push(newMenu);

			return newMenu;
		},
        deleteMenu: function(id){
		    let ids, index;
            ids = data.allItems.map(function(current) {

                return current.id;
            });

            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems.splice(index, 1);
            }
        }
	}


})();
const foods = (function () {

    let food = function (id,nId) {
        this.id = id;
        this.nId = nId;
    };
    let data = {
            allItems: [],
            items: []


    };
    return {
        addFood: function () {
            let newFood, ID,selectedID;
            selectedID = document.querySelector('.menuName').id;
            selectedID = selectedID.split('-');
            selectedID = parseInt(selectedID[1]);
            if (data.allItems.length > 0) {
                ID = data.allItems[data.allItems.length - 1].id + 1;
            }
            else {
                ID = 0;
                selectedID = 0;
            }
            newFood = new food(ID,selectedID);
            data.allItems.push(newFood);
            return newFood;
        },
        deleteFood: function(id){
            let ids, index;
            ids = data.allItems.map(function(current) {
                return current.id;
            });

            index = ids.indexOf(id);

            if (index !== -1) {
                data.allItems.splice(index, 1);
            }
        }
    };
})();
const Ui = (function(){
    const DOMstrings = {
        foodContainer: '#food',
    	foodAdd: '#foodAdd',
    	menuContainer: '.ulo',
        menuAddBtn: '#createMenu',
        menuDelBtn: '.ulo',
        menuSelect: '.ulo',
        bottomMenuName: '.menuName',
        tableMenus: '.tableMenus'
	};

    let take = [];
    let take2 = [];

    const checkIs = function(id){
	    if(take[id] === undefined)
        {
            take[id] = [];
        }
        if(take2[id] === undefined)
        {
            take2[id] = [];
        }
    };

	return {
        addListFood: function (obj,inp) {
            let html, element , element2, newHtml, html2, newHtml2;
            element = DOMstrings.foodContainer;
            element2 = DOMstrings.tableMenus;
            html = '<li class="hello" id="food-%nId%-%id%">%name% <ul> <a id="add2">prideti</a> <a id="del2">trinti</a> <a id="change2">taisyti</a> </ul></li>';
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%nId%', obj.nId);
            newHtml = newHtml.replace('%name%', inp);
            checkIs(obj.nId);
            take[obj.nId][obj.id] = newHtml;

            document.querySelector(element).innerHTML = take[obj.nId].join(" ");
            html2 = `<div class='tableMenu' id="tab-%nId%-%id%"><h1>%name%</h1><ul><li></li><li></li></ul></div>`;
            newHtml2 = html2.replace('%id%', obj.id);
            newHtml2 = newHtml2.replace('%nId%',obj.nId);
            newHtml2 = newHtml2.replace('%name%', inp);
            take2[obj.nId][obj.id] = newHtml2;
            document.querySelector(element2).innerHTML = take2[obj.nId].join(" ");
        },
        changeMenuName: function (firstName,secondName) {
            firstName.innerHTML = secondName;
        },
        addListMenu: function (obj,inp) {
            let html, element , newHtml;
            element = DOMstrings.menuContainer;
            html = '<li class="box" id="%id%"><a id="menu-%up%">%name%</a> <ul> <li><a id="add">prideti</a></li> <li><a id="del">trinti menu</a></li> <li><a id="change">taisyti</a></li> </ul> </li>';
            newHtml = html.replace('%id%', obj.id);
            newHtml = newHtml.replace('%name%', inp);
            newHtml = newHtml.replace('%up%', obj.id);
            document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },
        deleteListItem: function(selectorID) {
            const element = DOMstrings.foodContainer;
            const element2 = DOMstrings.tableMenus;
            const el = document.getElementById(selectorID);
            el.parentNode.removeChild(el);
            document.querySelector(element).innerHTML = take[selectorID] = '';
            document.querySelector(element2).innerHTML = take2[selectorID] = '';

        },
        deleteListFood: function(selectorID,firstID,id) {
            let first,second,split;

            const el = document.getElementById(selectorID);
            split = el.id.split('-');
            first = parseInt(split[1]);
            second = parseInt(split[2]);
            el.parentNode.removeChild(el);
            const el2 = document.getElementById(`tab-${first}-${second}`);
            el2.parentNode.removeChild(el2);
            if (id !== -1) {
                take[firstID].splice(id, 1);
                take2[firstID].splice(id, 1);

            }


        },
        changeBottom: function (selectedID,selectedName) {

            const element = DOMstrings.foodContainer;
            const element2 = DOMstrings.tableMenus;
            document.querySelector(".menuName").setAttribute('id',`nName-${selectedID}`);
            document.querySelector(".menuName").innerHTML = `<h2>${selectedName}</h2>`;
            checkIs(selectedID);
            document.querySelector(element).innerHTML = take[selectedID].join(" ");
            document.querySelector(element2).innerHTML = take2[selectedID].join(" ");

        },
        changeFoodName: function () {

        },
        getDOMstrings: function() {
            return DOMstrings;
        }
    };

})();

const app = (function(menuCtrl,UICtrl,foodCtrl){
    const setupEventListeners = function() {
        const DOM = UICtrl.getDOMstrings();
        document.querySelector(DOM.menuAddBtn).addEventListener('click',ctrlAddMenu);
        document.querySelector(DOM.foodAdd).addEventListener('click', ctrlAddFood2);
        document.querySelector(DOM.menuContainer).addEventListener('click', ctrlAddFood);
        document.querySelector(DOM.menuContainer).addEventListener('click', ctrlChangeItem);
        document.querySelector(DOM.menuSelect).addEventListener('click',selectMenu);
        document.querySelector(DOM.menuDelBtn).addEventListener('click',ctrlDeleteItem);
        document.querySelector(DOM.foodContainer).addEventListener('click',ctrlDeleteFood);
    };
    const ctrlAddMenu = function () {
        let newMenu;
        const name = prompt("name");
        if(name) {
            newMenu = menuCtrl.addMenu();
            UICtrl.addListMenu(newMenu, name);
        }
    };
    const ctrlAddFood = function (event) {
        let newFood;
        if(event.target.id === 'add')
        {
            const name = prompt("food pavadinimas");
            if(name){
                newFood = foodCtrl.addFood();
                UICtrl.addListFood(newFood,name);
            }
        }
    };
    const ctrlAddFood2 = function () {
        let newFood;
        const name = prompt("food pavadinimas");
        if (name) {
            newFood = foodCtrl.addFood();
            UICtrl.addListFood(newFood, name);
        }
    };
    const ctrlDeleteItem = function (event) {
        let itemID, ID,selectedID;
        selectedID = document.querySelector('.menuName').id;
        selectedID = selectedID.split('-');
        selectedID = selectedID[1];
        if(event.target.id === 'del') {
            itemID = event.target.parentNode.parentNode.parentNode.id;
        }
        if(itemID){
            const sure = prompt("are you sure want to delete? [y,n]");
            if(sure === 'y')
            {
                ID = parseInt(itemID);
                menuCtrl.deleteMenu(ID);
                UICtrl.deleteListItem(itemID);
                if(itemID === selectedID)
                {
                    document.querySelector('.menuName').innerHTML = '';
                }
            }
        }
    };
    const ctrlDeleteFood = function (event) {
        let itemID, ID,ID2,ids;
        if(event.target.id === 'del2') {
            itemID = event.target.parentNode.parentNode.id;

        }
        if(itemID){
            const sure = prompt("are you sure want to delete? [y,n]");
            if(sure === 'y')
            {
                ids = itemID.split('-');
                ID = parseInt(ids[1]);
                ID2 = parseInt(ids[2]);
                foodCtrl.deleteFood(ID);
                UICtrl.deleteListFood(itemID,ID,ID2);

            }
        }
    };
    const ctrlChangeItem = function (event) {
        let itemID;
        if(event.target.id === 'change') {
            const name = prompt("change name");
            itemID = event.target.parentNode.parentNode.parentNode.firstChild;
            UICtrl.changeMenuName(itemID,name);
        }
    };
    const selectMenu = function (event) {
        let ID,splitID;
        ID = event.target;
        splitID = ID.id.split('-');
        splitID = parseInt(splitID[1]);
        if(ID.id.includes('menu'))
        {
            UICtrl.changeBottom(splitID,ID.innerHTML);
        }
    };

    return {
        init: function() {
            console.log('Application has started.');
            setupEventListeners();
        }
    };

})(menus,Ui,foods);

app.init();
		